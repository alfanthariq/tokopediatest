package com.tokopedia.testproject

import android.app.Application
import android.content.Context
import com.tokopedia.testproject.problems.news.utils.AppDatabase
import com.tokopedia.testproject.problems.news.utils.GlideImageLoadingService
import ss.com.bannerslider.Slider

class ProjectApplication : Application() {
    companion object {
        var database: AppDatabase? = null

        fun getDB(context: Context) : AppDatabase?{
            ProjectApplication.database = AppDatabase.getInstance(context)
            return AppDatabase.getInstance(context)
        }
    }

    override fun onCreate() {
        super.onCreate()

        getDB(this)

        val imageService = GlideImageLoadingService(applicationContext)
        Slider.init(imageService)
    }
}