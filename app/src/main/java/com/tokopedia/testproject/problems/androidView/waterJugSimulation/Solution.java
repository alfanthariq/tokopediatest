package com.tokopedia.testproject.problems.androidView.waterJugSimulation;

import java.util.ArrayList;
import java.util.List;

public class Solution {
    private static List<WaterJugAction> list;

    public static List<WaterJugAction> simulateWaterJug(int jug1, int jug2, int target) {
        // TODO, simulate the smallest number of action to do the water jug problem
        // below is stub, replace with your implementation!
        list = new ArrayList<>();

        minSteps(jug1, jug2, target);
        list.size();

        return list;
    }

    private static int gcd(int a, int b)
    {
        if (b==0)
            return a;
        return gcd(b, a%b);
    }

    /* fromCap -- Capacity of jug from which
                  water is poured
       toCap   -- Capacity of jug to which
                  water is poured
       d       -- Amount to be measured */
    private static int pour(int fromCap, int toCap, int d)
    {
        // Initialize current amount of water
        // in source and destination jugs
        int from = fromCap;
        int to = 0;

        // Initialize count of steps required
        int step = 1; // Needed to fill "from" Jug
        int pourCount = 0;

        // Break the loop when either of the two
        // jugs has d litre water
        while (from != d && to != d)
        {
            // Find the maximum amount that can be
            // poured
            int temp = Math.min(from, toCap - to);

            // Pour "temp" litres from "from" to "to"
            to   += temp;
            from -= temp;
            list.add(new WaterJugAction(WaterJugActionEnum.POUR, 2));
            pourCount++;

            // Increment count of steps
            step++;

            if (from == d || to == d)
                break;

            // If first jug becomes empty, fill it
            if (from == 0)
            {
                from = fromCap;
                list.add(new WaterJugAction(WaterJugActionEnum.FILL, 1));
                step++;
            }

            // If second jug becomes full, empty it
            if (to == toCap)
            {
                to = 0;
                list.add(new WaterJugAction(WaterJugActionEnum.EMPTY, 2));
                step++;
            }
        }
        System.out.println("Step : "+step);
        return step;
    }

    // Returns count of minimum steps needed to
// measure d litre
    private static int minSteps(int m, int n, int d)
    {
        // To make sure that m is smaller than n
        if (m > n)
            swap(m, n);

        // For d > n we cant measure the water
        // using the jugs
        if (d > n)
            return -1;

        // If gcd of n and m does not divide d
        // then solution is not possible
        if ((d % gcd(n,m)) != 0)
            return -1;

        // Return minimum two cases:
        // a) Water of n litre jug is poured into
        //    m litre jug
        // b) Vice versa of "a"
        int a = pour(n,m,d);
        int b = pour(m,n,d);

        list.clear();
        list.add(new WaterJugAction(WaterJugActionEnum.FILL, 1));
        if (a > b) {
            return pour(m,n,d);
        } else {
            return pour(n,m,d);
        }
    }

    private static void swap(int x, int y) {
        int a = x*y;
        int _x = x;
        x = a / y;
        y = a / _x;
    }
}
