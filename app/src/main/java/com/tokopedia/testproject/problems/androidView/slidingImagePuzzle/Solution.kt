package com.tokopedia.testproject.problems.androidView.slidingImagePuzzle

import android.content.Context
import android.graphics.Bitmap
import android.graphics.BitmapFactory

import com.tokopedia.testproject.R
import com.tokopedia.testproject.*
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

import java.io.IOException
import java.net.URL
import java.util.ArrayList
import java.util.Arrays

object Solution {
    interface onSuccessLoadBitmap {
        fun onSliceSuccess(bitmapList: List<Bitmap>)
        fun onSliceFailed(throwable: Throwable)
    }

    fun sliceTo4x4(context: Context, onSuccessLoadBitmap: onSuccessLoadBitmap, imageUrl: String) {
        val bitmapList = ArrayList<Bitmap>()
        // TODO, download the image, crop, then sliced to 15 Bitmap (4x4 Bitmap). ignore the last Bitmap
        // below is stub, replace with your implementation!
        /*bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample11));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample12));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample13));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample14));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample21));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample22));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample23));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample24));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample31));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample32));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample33));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample34));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample41));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample42));
        bitmapList.add(UtilKt.toBitmap(context, R.drawable.sample43));*/
        try {
            doAsync {
                val url = URL(imageUrl)
                val image = BitmapFactory.decodeStream(url.openConnection().getInputStream())

                val cropped = if (image.width >= image.height){
                    Bitmap.createBitmap(
                            image,
                            image.width/2 - image.height/2,
                            0,
                            image.height,
                            image.height
                    )
                }else{
                    Bitmap.createBitmap(
                            image,
                            0,
                            image.height/2 - image.width/2,
                            image.width,
                            image.width
                    )
                }

                bitmapList.clear()
                bitmapList.addAll(splitBitmapArray(cropped, 4, 4))

                uiThread {
                    onSuccessLoadBitmap.onSliceSuccess(bitmapList)
                }
            }
        } catch (e: IOException) {
            onSuccessLoadBitmap.onSliceFailed(e)
            println(e)
        }
    }

    fun splitBitmapArray(bitmap: Bitmap, xCount: Int, yCount: Int): ArrayList<Bitmap> {
        val bitmaps = ArrayList<Bitmap>()
        val width = bitmap.width / xCount
        val height = bitmap.height / yCount
        for (y in 0 until xCount) {
            for (x in 0 until yCount) {
                // Create the sliced bitmap
                //bitmaps[x][y] = Bitmap.createBitmap(bitmap, x * width, y * height, width, height)
                bitmaps.add(Bitmap.createBitmap(bitmap, x * width, y * height, width, height))
            }
        }
        // Return the array
        return bitmaps
    }
}
