package com.tokopedia.testproject.problems.news.model

import android.arch.persistence.room.*

@Entity(tableName = "news")
data class News(
        @PrimaryKey(autoGenerate = true)
        @ColumnInfo(name = "id")
        var id: Int = 0,
        @ColumnInfo(name = "author")
        var author: String = "",
        @ColumnInfo(name = "title")
        var title: String = "",
        @ColumnInfo(name = "description")
        var description: String = "",
        @ColumnInfo(name = "url")
        var url: String = "",
        @ColumnInfo(name = "urlToImage")
        var urlToImage: String = "",
        @ColumnInfo(name = "publishedAt")
        var publishedAt: String = "",
        @ColumnInfo(name = "content")
        var content: String = "",
        @ColumnInfo(name = "page")
        var page: Int = 0
)
{
    @Ignore
    constructor() : this(0)
}

@Dao
interface NewsDAO {
    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insert(data: News)

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun insertLists(lists: List<News>)

    @Query("SELECT * FROM news ORDER BY publishedAt DESC")
    fun all(): List<News>

    @Query("SELECT * FROM news WHERE page = :page ORDER BY publishedAt DESC")
    fun byPage(page : Int): List<News>

    @Query("SELECT * FROM news WHERE id = :id")
    fun one(id : Int): News

    @Query("DELETE FROM news")
    fun deleteAll()
}