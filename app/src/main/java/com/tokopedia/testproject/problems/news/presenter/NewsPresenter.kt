package com.tokopedia.testproject.problems.news.presenter

import android.content.Context

import com.tokopedia.testproject.ProjectApplication
import com.tokopedia.testproject.problems.news.model.Article
import com.tokopedia.testproject.problems.news.model.News
import com.tokopedia.testproject.problems.news.model.NewsList
import com.tokopedia.testproject.problems.news.model.NewsResult
import com.tokopedia.testproject.problems.news.network.NewsDataSource
import com.tokopedia.testproject.problems.news.utils.AppDatabase
import com.tokopedia.testproject.problems.news.utils.DateOperationUtil

import io.reactivex.Observer
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers
import org.jetbrains.anko.doAsync
import org.jetbrains.anko.uiThread

/**
 * Created by hendry on 27/01/19.
 */
class NewsPresenter(view: NewsPresenter.View, private val mContext: Context) {

    private val composite = CompositeDisposable()
    private val apikey = "cf5a8b9647994d1c9d0348a4d15fb6dd"
    private var db: AppDatabase = ProjectApplication.getDB(mContext)!!

    private val view: View = view

    interface View {
        fun onSuccessGetNews(articleList: List<NewsList>?, total: Int)

        fun onErrorGetNews(throwable: Throwable)

        fun onSuccessGetHeadline(articleList: List<Article>?, total: Int)

        fun onErrorGetHeadline(throwable: Throwable)
    }

    fun getEverything(keyword: String, page: Int) {
        NewsDataSource.getService().getEverything(keyword, "publishedAt", page, "2019-04-05", apikey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<NewsResult> {
                    override fun onSubscribe(d: Disposable) {
                        composite.add(d)
                    }

                    override fun onNext(newsResult: NewsResult) {
                        val data = ArrayList<News>()
                        newsResult.articles?.forEachIndexed { idx, it ->
                            val news = News()
                            news.author = it.author ?: ""
                            news.title = it.title ?: ""
                            news.description = it.description ?: ""
                            news.url = it.url ?: ""
                            news.urlToImage = it.urlToImage ?: ""
                            val tgl = it.publishedAt ?: DateOperationUtil.getCurrentTimeStr("yyyy-MM-dd'T'HH:mm:ss'Z'")
                            val date = DateOperationUtil.dateStrFormat("yyyy-MM-dd'T'HH:mm:ss'Z'", "yyyy-MM-dd HH:mm:ss", tgl)
                            news.publishedAt = date //if (idx >= newsResult.articles.size - 5) "2019-03-25 12:30" else date
                            news.content = it.content ?: ""
                            news.page = page

                            data.add(news)
                        }

                        doAsync {
                            db.newsDAO().deleteAll()
                            db.newsDAO().insertLists(data)

                            uiThread {
                                loadNewsGroup(page, newsResult.totalResults)
                            }
                        }
                        //view.onSuccessGetNews(newsResult.articles, newsResult.totalResults)
                    }

                    override fun onError(e: Throwable) {
                        view.onErrorGetNews(e)
                    }

                    override fun onComplete() {

                    }
                })
    }

    fun getHeadline(country: String) {
        NewsDataSource.getService().getHeadlines(country, 5, apikey)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(object : Observer<NewsResult> {
                    override fun onSubscribe(d: Disposable) {
                        composite.add(d)
                    }

                    override fun onNext(newsResult: NewsResult) {
                        view.onSuccessGetHeadline(newsResult.articles, newsResult.totalResults)
                    }

                    override fun onError(e: Throwable) {
                        view.onErrorGetHeadline(e)
                    }

                    override fun onComplete() {

                    }
                })
    }

    fun loadNewsGroup(page : Int, total : Int) {
        doAsync {
            val datas = ArrayList<NewsList>()

            val dataAll = db.newsDAO().all()
            val tot = if (total == -1) {
                dataAll.size
            } else {
                total
            }

            dataAll.forEach {
                val data = NewsList()
                val bulanTahunData = DateOperationUtil.dateStrFormat("yyyy-MM-dd", "MMM yyyy", it.publishedAt)
                val tglPublish = DateOperationUtil.dateStrFormat("yyyy-MM-dd HH:mm", "dd MMMM yyyy HH:mm", it.publishedAt)

                if (datas.isEmpty()) {
                    if (page <= 1) {
                        val dataDump = NewsList()
                        dataDump.id = -1
                        dataDump.bulanTahun = bulanTahunData
                        dataDump.dataType = 0
                        datas.add(dataDump)
                    }

                    data.id = it.id
                    data.title = it.title
                    data.description = it.description
                    data.publishedAt = tglPublish
                    data.imgUrl = it.urlToImage
                    data.bulanTahun = bulanTahunData
                    data.dataType = 1
                    datas.add(data)
                } else {
                    if (bulanTahunData != datas[datas.lastIndex].bulanTahun) {
                        val dataDump = NewsList()
                        dataDump.id = -1
                        dataDump.bulanTahun = bulanTahunData
                        dataDump.dataType = 0
                        datas.add(dataDump)

                        data.id = it.id
                        data.title = it.title
                        data.description = it.description
                        data.publishedAt = tglPublish
                        data.imgUrl = it.urlToImage
                        data.bulanTahun = bulanTahunData
                        data.dataType = 1
                        datas.add(data)
                    } else {
                        data.id = it.id
                        data.title = it.title
                        data.description = it.description
                        data.publishedAt = tglPublish
                        data.imgUrl = it.urlToImage
                        data.bulanTahun = bulanTahunData
                        data.dataType = 1
                        datas.add(data)
                    }
                }
            }

            uiThread {
                view.onSuccessGetNews(datas, tot)
            }
        }
    }

    fun unsubscribe() {
        composite.dispose()
    }
}
