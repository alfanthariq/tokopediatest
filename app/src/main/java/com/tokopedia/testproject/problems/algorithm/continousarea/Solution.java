package com.tokopedia.testproject.problems.algorithm.continousarea;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by hendry on 18/01/19.
 */
public class Solution {
    static ArrayList<Data> visited = new ArrayList<>();
    static int con = 0;

    public static int maxContinuousArea(int[][] matrix) {
        // TODO, return the largest continuous area containing the same integer, given the 2D array with integers
        // below is stub

        ArrayList<Integer> continous = new ArrayList<>();
        int rowCount = matrix.length;
        for (int i = 0; i < rowCount; i++){
            int colCount = matrix[i].length;
            for (int j = 0; j < colCount; j++) {
                con = 0;
                int val = matrix[i][j];
                if (findData(visited, i, j) == null) {
                    checkSame(val, matrix, i, j, false, false);

                    continous.add(con+1);
                }
            }
        }

        Collections.sort(continous, Collections.<Integer>reverseOrder());

        int hasil = 0;
        if (!continous.isEmpty()) {
            hasil = continous.get(0);
        }
        return hasil;
    }

    static void checkSame(int val, int[][] matrix, int i, int j, boolean right, boolean left){
        if ((j+1 < matrix[i].length) && (!left)) {
            if (matrix[i][j+1] == val) {
                if (findData(visited, i, j+1) == null) {
                    Data d1 = new Data(i, j+1);
                    visited.add(d1);

                    con += 1;
                    int x = i;
                    int y = j+1;
                    checkSame(val, matrix, i, j+1, true, false);
                }

            }
        }

        if ((j-1 > 0) && (!right)) {
            if (matrix[i][j-1] == val) {
                if (findData(visited, i, j-1) == null) {
                    Data d1 = new Data(i, j-1);
                    visited.add(d1);

                    con += 1;
                    int x = i;
                    int y = j-1;
                    checkSame(val, matrix, i, j-1, false, true);
                }

            }
        }

        if (i+1 < matrix.length) {
            if (matrix[i+1][j] == val) {
                if (findData(visited, i+1, j) == null) {
                    Data d1 = new Data(i+1, j);
                    visited.add(d1);

                    con += 1;
                    int x = i+1;
                    int y = j;
                    checkSame(val, matrix, i+1, j, false, false);
                }

            }
        }
    }

    static Data findData(ArrayList<Data> datas, int x, int y){
        for (Data data : datas) {
            if ((data.x == x) && (data.y == y)) {
                return data;
            }
        }

        return null;
    }

    static class Data {
        int x, y;
        public Data(int x, int y) {
            this.x = x;
            this.y = y;
        }

        public int getX() {
            return x;
        }

        public int getY() {
            return y;
        }
    }
}
