package com.tokopedia.testproject.problems.algorithm.maxrectangle;

import java.util.Stack;

public class Solution {

    static int maxHist(int col,int row[])
    {
        Stack<Integer> result = new Stack<>();
        int top_val;
        int max_area = 0;
        int area = 0;
        int i = 0;
        while (i < col)
        {
            if (result.empty() || row[result.peek()] <= row[i])
                result.push(i++);

            else
            {
                top_val = row[result.peek()];
                result.pop();
                area = top_val * i;

                if (!result.empty())
                    area = top_val * (i - result.peek() - 1 );
                max_area = Math.max(area, max_area);
            }
        }

        while (!result.empty())
        {
            top_val = row[result.peek()];
            result.pop();
            area = top_val * i;
            if (!result.empty())
                area = top_val * (i - result.peek() - 1 );

            max_area = Math.max(area, max_area);
        }
        return max_area;
    }

    public static int maxRect(int[][] matrix) {
        // TODO, return the largest area containing 1's, given the 2D array of 0s and 1s
        // below is stub

        int row = matrix.length;
        int result = maxHist(matrix[0].length,matrix[0]);

        for (int i = 1; i < row; i++)
        {
            int col = matrix[i].length;
            for (int j = 0; j < col; j++)
                if (matrix[i][j] == 1) matrix[i][j] += matrix[i - 1][j];

            result = Math.max(result, maxHist(col,matrix[i]));
        }

        return result;
    }

    public static int maxSquare(int[][] matrix) {
        int i,j;
        int rows = matrix.length;
        int cols = matrix[0].length;
        int temp[][] = new int[rows][cols];
        int maxCount = 0;

        int max_temp, max_i, max_j;

        // copy row ke temp
        for(i = 0; i < rows; i++)
            temp[i][0] = matrix[i][0];

        // copy col ke temp
        for(j = 0; j < cols; j++)
            temp[0][j] = matrix[0][j];

        // copy isi ke temp
        for(i = 1; i < rows; i++)
        {
            for(j = 1; j < cols; j++)
            {
                if(matrix[i][j] == 1)
                    temp[i][j] = Math.min(temp[i][j-1],
                            Math.min(temp[i-1][j], temp[i-1][j-1])) + 1;
                else
                    temp[i][j] = 0;
            }
        }

        // cari value tertinggi di temp
        max_temp = temp[0][0]; max_i = 0; max_j = 0;
        for(i = 0; i < rows; i++)
        {
            for(j = 0; j < cols; j++)
            {
                if(max_temp < temp[i][j])
                {
                    max_temp = temp[i][j];
                    max_i = i;
                    max_j = j;
                }
            }
        }

        // hitung jumlah value tertinggi
        for(i = 0; i < rows; i++)
        {
            for(j = 0; j < cols; j++)
            {
                if(temp[i][j] == max_temp)
                {
                    maxCount += 1;
                }
            }
        }

        System.out.println("Max i : "+max_i+", Max j : "+max_j+", Max temp : "+max_temp);
        int hasil = 0;
        int loopJ;

        // cek hasil sub matrix square atau
        if (maxCount <= 1) loopJ = max_j - max_temp; else loopJ = (max_j - max_temp) - 1;

        // hitung hasil
        for(i = max_i; i > max_i - max_temp; i--)
        {
            for(j = max_j; j > loopJ; j--)
            {
                hasil += 1;
            }
        }

        return hasil;
    }
}
