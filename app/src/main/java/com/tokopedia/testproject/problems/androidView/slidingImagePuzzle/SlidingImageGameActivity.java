package com.tokopedia.testproject.problems.androidView.slidingImagePuzzle;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayout;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.tokopedia.testproject.R;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

public class SlidingImageGameActivity extends AppCompatActivity {
    public static final String X_IMAGE_URL = "x_image_url";
    public static final int GRID_NO = 4;
    private String imageUrl;
    ImageView[][] imageViews = new ImageView[4][4];
    private GridLayout gridLayout;
    private int xBlank, yBlank, xSel, ySel;
    private ArrayList<Integer> oriPuzzle, shuffledPuzzle;

    public static Intent getIntent(Context context, String imageUrl) {
        Intent intent = new Intent(context, SlidingImageGameActivity.class);
        intent.putExtra(X_IMAGE_URL, imageUrl);
        return intent;
    }

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        imageUrl = getIntent().getStringExtra(X_IMAGE_URL);
        setContentView(R.layout.activity_sliding_image_game);
        gridLayout = findViewById(R.id.gridLayout);
        xBlank = GRID_NO-1; yBlank = GRID_NO-1;
        oriPuzzle = new ArrayList<>();
        shuffledPuzzle = new ArrayList<>();

        LayoutInflater inflater = LayoutInflater.from(this);
        for (int i = 0; i < GRID_NO; i++) {
            for (int j = 0; j < GRID_NO; j++) {
                ImageView view = (ImageView) inflater.inflate(R.layout.item_image_sliding_image,
                        gridLayout, false);
                gridLayout.addView(view);
                final int x = i;
                final int y = j;
                imageViews[i][j] = view;
                imageViews[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        xSel = x;
                        ySel = y;
                        boolean valid = false;
                        if ((xSel != xBlank) && (ySel == yBlank)) {
                            if (Math.abs(xBlank-xSel) == 1) {
                                valid = true;
                            }
                        } else
                        if ((ySel != yBlank) && (xSel == xBlank)) {
                            if (Math.abs(yBlank-ySel) == 1) {
                                valid = true;
                            }
                        }

                        if (valid) {
                            Bitmap bitmap = ((BitmapDrawable)imageViews[xSel][ySel].getDrawable()).getBitmap();
                            imageViews[xBlank][yBlank].setImageBitmap(bitmap);
                            imageViews[xSel][ySel].setImageBitmap(null);

                            if ((xSel*GRID_NO) + ySel < (GRID_NO*2)-1) {
                                int oldVal = shuffledPuzzle.get((xSel*GRID_NO) + ySel);
                                shuffledPuzzle.set((xSel*GRID_NO) + ySel, shuffledPuzzle.get((xBlank*GRID_NO) + yBlank));
                                shuffledPuzzle.set((xBlank*GRID_NO) + yBlank, oldVal);
                            }

                            xBlank = xSel;
                            yBlank = ySel;

                            if (checkEquality(oriPuzzle, shuffledPuzzle)) {
                                new AlertDialog.Builder(SlidingImageGameActivity.this)
                                        .setTitle("Solved")
                                        .setMessage("Awesome, you've solved the puzzle")
                                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                                            public void onClick(DialogInterface dialog, int which) {
                                                dialog.dismiss();
                                            }
                                        })
                                        .setIcon(android.R.drawable.ic_dialog_info)
                                        .show();
                            }
                        }
                    }
                });
            }
        }

        Solution.INSTANCE.sliceTo4x4(this, new Solution.onSuccessLoadBitmap() {
            @Override
            public void onSliceSuccess(List<Bitmap> bitmapList) {
                //TODO will randomize placement to grid. Note: the game must be solvable.
                //replace below implementation to your implementation.
                int counter = 0;
                // Generate original array
                ArrayList<Integer> index = new ArrayList<>();
                bitmapList.remove(bitmapList.size()-1);
                for (int i = 0; i < bitmapList.size(); i++){
                    index.add(i);
                }
                oriPuzzle.addAll(index);

                // Generate shuffled array
                /*ArrayList<Integer> temp = new ArrayList<>();
                index.clear();
                for (int i = 0; i < GRID_NO; i++){
                    temp.clear();
                    for (int j = 0; j < GRID_NO; j++){
                        temp.add((i*GRID_NO) + j);
                    }
                    Collections.shuffle(temp);
                    index.addAll(temp);
                }*/
                Collections.shuffle(index);
                shuffledPuzzle.addAll(index);

                for (int i = 0; i < GRID_NO; i++) {
                    for (int j = 0; j < GRID_NO; j++) {
//                        int select = r.nextInt(index.size());
//                        int random = index.get(select);
//                        index.remove(select);

                        if ((i == GRID_NO-1) && (j == GRID_NO-1)) {
                            imageViews[i][j].setImageBitmap(null);
                        } else {
                            if (index.get(counter) < bitmapList.size()) {
                                imageViews[i][j].setImageBitmap(bitmapList.get(index.get(counter)));
                            }
                        }
                        counter++;
                    }
                }
            }

            @Override
            public void onSliceFailed(Throwable throwable) {
                Toast.makeText(SlidingImageGameActivity.this,
                        throwable.getMessage(), Toast.LENGTH_LONG).show();
            }
        }, imageUrl);

        // TODO add implementation of the game.
        // There is image adjacent to blank space (either horizontal or vertical).
        // If that image is clicked, it will swap to the blank space
        // if the puzzle is solved (the image in the view is aligned with the original image), then show a "success" dialog

        // TODO add handling for rotation to save the user input.
        // If the device is rotated, it should retain user's input, so user can continue the game.
    }

    boolean checkEquality(ArrayList<Integer> a, ArrayList<Integer> b) {
        boolean equal = true;
        for (int i = 0; i < a.size(); i++) {
            if (a.get(i) != b.get(i)) {
                equal = false;
                return equal;
            }
        }
        return equal;
    }
}
