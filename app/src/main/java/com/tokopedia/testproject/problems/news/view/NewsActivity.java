package com.tokopedia.testproject.problems.news.view;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.support.v7.widget.SearchView;
import android.widget.TextView;
import com.tokopedia.testproject.R;
import com.tokopedia.testproject.problems.news.model.Article;
import com.tokopedia.testproject.problems.news.model.News;
import com.tokopedia.testproject.problems.news.model.NewsList;
import com.tokopedia.testproject.problems.news.presenter.NewsPresenter;
import com.tokopedia.testproject.problems.news.utils.EndlessRecyclerViewScrollListener;
import com.tokopedia.testproject.problems.news.utils.NetworkUtil;

import java.util.ArrayList;
import java.util.List;

import ss.com.bannerslider.Slider;

public class NewsActivity extends AppCompatActivity implements
        com.tokopedia.testproject.problems.news.presenter.NewsPresenter.View,
        ErrorView.ErrorListener {

    private NewsPresenter newsPresenter;
    private NewsAdapter newsAdapter;
    private RelativeLayout lyt_waiting;
    private TextView txt_loading;
    private RecyclerView recyclerView;
    private LinearLayout lyt_empty;
    private ErrorView lyt_error;
    private EndlessRecyclerViewScrollListener endlessListener;
    private int last_page, totalPage;
    private String last_query = "android";
    private ArrayList<NewsList> articleList;
    private boolean isLoading = false;
    private SearchView searchView;
    private Slider slider;
    private ArrayList<Article> bannerArticles;
    private NewsSliderAdapter bannerAdapter;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_news);
        newsPresenter = new NewsPresenter(this, this);
        newsAdapter = new NewsAdapter(null);
        recyclerView = findViewById(R.id.recyclerView);
        lyt_waiting = findViewById(R.id.lyt_waiting);
        txt_loading = findViewById(R.id.txt_loading);
        lyt_empty = findViewById(R.id.lyt_empty);
        lyt_error = findViewById(R.id.lyt_error);
        last_page = 1;
        slider = findViewById(R.id.banner_slider);
        bannerArticles = new ArrayList<>();

        setupRecycler();
        newsPresenter.getHeadline("id");

        initData();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate( R.menu.news_menu, menu);

        final MenuItem myActionMenuItem = menu.findItem( R.id.mCari);
        searchView = (SearchView) myActionMenuItem.getActionView();
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                // Toast like print
                last_page=1;
                last_query = query;
                searchView.setQuery(last_query, false);
                setupRecycler();
                initData();
                if( ! searchView.isIconified()) {
                    searchView.setIconified(true);
                }
                myActionMenuItem.collapseActionView();
                return false;
            }
            @Override
            public boolean onQueryTextChange(String s) {
                return false;
            }
        });

        return true;
    }

    private void loadMore(int page) {
        if (!isLoading) {
            isLoading = true;
            newsAdapter.addLoading();
            if (NetworkUtil.INSTANCE.isNetworkConnected(NewsActivity.this)) {
                newsPresenter.getEverything(last_query, page);
            } else {
                newsPresenter.loadNewsGroup(last_page, -1);
            }
        }
    }

    private void setupRecycler(){
        articleList = new ArrayList<>();
        LinearLayoutManager lm = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(lm);

        recyclerView.setAdapter(newsAdapter);

        endlessListener = new EndlessRecyclerViewScrollListener(lm) {
            @Override
            public void onLoadMore(int page, int totalItemsCount, RecyclerView view) {
                last_page += 1;
                double page_load = 20.0;
                int maxPage = (int) Math.ceil((double) totalPage / page_load);
                if (last_page<=maxPage) {
                    loadMore(last_page);
                }
            }
        };
        recyclerView.addOnScrollListener(endlessListener);
    }

    private void setupBanner(){
        bannerAdapter = new NewsSliderAdapter();
        bannerAdapter.setArticles(bannerArticles);
        slider.setAdapter(bannerAdapter);
        slider.setInterval(5000);
    }

    private void initData(){
        setLoading(true, "Loading news ...");
        articleList.clear();
        if (NetworkUtil.INSTANCE.isNetworkConnected(this)) {
            newsPresenter.getEverything(last_query, 1);
        } else {
            newsPresenter.loadNewsGroup(last_page,-1);
        }
    }

    @Override
    public void onSuccessGetNews(List<NewsList> articleList, int total) {
        totalPage = total;
        newsAdapter.removeLoading();
        this.articleList.addAll(articleList);
        newsAdapter.setArticleList(this.articleList);
        newsAdapter.notifyDataSetChanged();
        setLoading(false, "");

        if (articleList.isEmpty()) {
            setEmpty(true);
        }
        isLoading = false;
    }

    @Override
    public void onErrorGetNews(Throwable throwable) {
        lyt_empty.setVisibility(View.GONE);
        lyt_waiting.setVisibility(View.GONE);
        lyt_error.setView(true, throwable.getMessage(), 0, this);
        recyclerView.setVisibility(View.GONE);
        isLoading = false;
    }

    @Override
    public void onSuccessGetHeadline(List<Article> articleList, int total) {
        bannerArticles.clear();
        bannerArticles.addAll(articleList);
        setupBanner();
    }

    @Override
    public void onErrorGetHeadline(Throwable throwable) {

    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        newsPresenter.unsubscribe();
    }

    private void setLoading(boolean showing, String msg) {
        lyt_error.setView(false, "", 0, this);
        lyt_empty.setVisibility(View.GONE);
        if (showing) {
            lyt_waiting.setVisibility(View.VISIBLE);
            txt_loading.setText(msg);
            recyclerView.setVisibility(View.GONE);
        } else {
            lyt_waiting.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    private void setEmpty(boolean showing) {
        if (showing) {
            lyt_empty.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        } else {
            lyt_empty.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onReloadData() {
        last_page = 1;
        setupRecycler();
        initData();
    }
}
