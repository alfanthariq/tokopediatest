package com.tokopedia.testproject.problems.news.model

class NewsList {
    var id = 0
    var title = ""
    var description = ""
    var publishedAt = ""
    var imgUrl = ""
    var dataType = 0
    var bulanTahun = ""
}