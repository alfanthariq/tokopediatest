package com.tokopedia.testproject.problems.news.network;

import com.tokopedia.testproject.problems.news.model.NewsResult;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface NewsService {
    @GET("everything")
    Observable<NewsResult> getEverything(@Query("q") String query,
                                         @Query("sortBy") String sort,
                                         @Query("page") int page,
                                         @Query("from") String from,
                                         @Query("apiKey") String apikey);

    @GET("top-headlines")
    Observable<NewsResult> getHeadlines(@Query("country") String country,
                                         @Query("pageSize") int page,
                                         @Query("apiKey") String apikey);
}
