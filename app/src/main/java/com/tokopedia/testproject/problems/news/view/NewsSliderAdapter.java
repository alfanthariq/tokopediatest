package com.tokopedia.testproject.problems.news.view;

import com.tokopedia.testproject.problems.news.model.Article;

import java.util.List;

import ss.com.bannerslider.adapters.SliderAdapter;
import ss.com.bannerslider.viewholder.ImageSlideViewHolder;

public class NewsSliderAdapter extends SliderAdapter {
    private List<Article> articles;

    public void setArticles (List<Article> list) {
        articles = list;
    }

    @Override
    public int getItemCount() {
        return articles.size();
    }

    @Override
    public void onBindImageSlide(int position, ImageSlideViewHolder viewHolder) {
        viewHolder.bindImageSlide(articles.get(position).getUrlToImage());
    }
}
