package com.tokopedia.testproject.problems.news.utils

import android.arch.persistence.db.SupportSQLiteDatabase
import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.arch.persistence.room.migration.Migration
import android.content.Context
import com.tokopedia.testproject.problems.news.model.News
import com.tokopedia.testproject.problems.news.model.NewsDAO

@Database(entities = [News::class], version = 1)
abstract class AppDatabase : RoomDatabase() {

    abstract fun newsDAO(): NewsDAO

    companion object {
        private val databaseName = "data.db"
        var database: AppDatabase? = null

        fun getInstance(context: Context): AppDatabase? {
            if (database == null) {
                synchronized(AppDatabase::class.java) {
                    if (database == null) {
                        database = buildDatabase(context)
                    }
                }
            }
            return database
        }

        fun buildDatabase(applicationContext: Context): AppDatabase? {
            return Room.databaseBuilder(applicationContext, AppDatabase::class.java, databaseName)
                    //.addMigrations(MIGRATION_1_2)
                    .build()
        }

        private val MIGRATION_1_2 = object : Migration(1, 2) {
            override fun migrate(database: SupportSQLiteDatabase) {
                database.execSQL("CREATE TABLE finger_upt (" +
                        "    upt_code TEXT NOT NULL," +
                        "    upt_name TEXT," +
                        "    upt_ip TEXT," +
                        "    status INTEGER NOT NULL," +
                        "    PRIMARY KEY (" +
                        "        upt_code" +
                        "    )" +
                        ");")
            }
        }
    }

}