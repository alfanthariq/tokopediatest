package com.tokopedia.testproject.problems.androidView.graphTraversal;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.CardView;
import android.view.View;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.tokopedia.testproject.R;

import java.util.List;

import de.blox.graphview.BaseGraphAdapter;
import de.blox.graphview.Graph;
import de.blox.graphview.GraphView;
import de.blox.graphview.Node;
import de.blox.graphview.NodeData;
import de.blox.graphview.energy.FruchtermanReingoldAlgorithm;

public class GraphActivity extends AppCompatActivity {
    private int nodeCount = 1;
    private Node currentNode;
    protected BaseGraphAdapter<ViewHolder> adapter;
    private int target;
    int level = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_graph);

        final Graph graph = createGraph();
        setupAdapter(graph);
    }

    private void setupAdapter(Graph graph) {
        final GraphView graphView = findViewById(R.id.graph2);

        adapter = new BaseGraphAdapter<ViewHolder>(this, R.layout.node, graph) {
            @NonNull
            @Override
            public ViewHolder onCreateViewHolder(View view) {
                return new ViewHolder(view);
            }

            @Override
            public void onBindViewHolder(ViewHolder viewHolder, NodeData data, int position) {
                viewHolder.textView.setText(data.getNama());
                if ((data.getLevel() < target) && (data.getLevel() > 0)) {
                    viewHolder.cardView.setBackgroundColor(getResources().getColor(R.color.green_500));
                } else if (data.getLevel() == target) {
                    viewHolder.cardView.setBackgroundColor(getResources().getColor(R.color.orange_500));
                } else if (data.getLevel() == 0) {
                    viewHolder.cardView.setBackgroundColor(getResources().getColor(R.color.purple_500));
                } else {
                    viewHolder.cardView.setBackgroundColor(getResources().getColor(R.color.light_blue_500));
                }
            }
        };

        adapter.setAlgorithm(new FruchtermanReingoldAlgorithm());

        graphView.setAdapter(adapter);
        graphView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                currentNode = adapter.getNode(position);
                adapter.notifyDataChanged(currentNode);
                Snackbar.make(graphView, "Clicked on " + currentNode.getData().getNama(), Toast.LENGTH_SHORT).show();
            }
        });

        /*
         * TODO
         * Given input:
         * 1. a graph that represents a tree (there is no cyclic node),
         * 2. a rootNode, and
         * 3. a target distance,
         * you have to traverse the graph and give the color to each node as below criteria:
         * 1. RootNode is purple
         * 2. Nodes with the distance are less than the target distance are colored green
         * 3. Nodes with the distance are equal to the target distance are colored orange
         * 4. Other Nodes are blue
         */

        traverseAndColorTheGraph(graph, graph.getNode(0), 2);
    }

    private void traverseAndColorTheGraph(Graph graph, Node rootNode, int target) {
        this.target = target;
        NodeData ndRoot = rootNode.getData();
        ndRoot.setLevel(0);
        rootNode.setData(ndRoot);
        List<Node> nodess = graph.successorsOf(graph.getNode(1));
        System.out.println(nodess);
        getLevel(graph, rootNode);
    }

    private void getLevel(Graph graph, Node node) {
        List<Node> nodes = graph.successorsOf(node);
        if (nodes.size() > 0) {
            for (Node n : nodes) {
                NodeData nd = n.getData();
                nd.setLevel(level);
                n.setData(nd);
                if (graph.hasSuccessor(n)) level++;
                getLevel(graph, n);
            }
            level --;
        }
    }

    @Override
    public boolean onSupportNavigateUp() {
        onBackPressed();
        return true;
    }

    public Graph createGraph() {
        final Graph graph = new Graph();

        final Node a = new Node(new NodeData(getNodeText()));
        final Node b = new Node(new NodeData(getNodeText()));
        final Node c = new Node(new NodeData(getNodeText()));
        final Node d = new Node(new NodeData(getNodeText()));
        final Node e = new Node(new NodeData(getNodeText()));
        final Node f = new Node(new NodeData(getNodeText()));
        final Node g = new Node(new NodeData(getNodeText()));
        final Node h = new Node(new NodeData(getNodeText()));
        final Node i = new Node(new NodeData(getNodeText()));

        graph.addEdge(a, b);
        graph.addEdge(a, c);
        graph.addEdge(b, f);
        graph.addEdge(b, g);
        graph.addEdge(b, h);
        graph.addEdge(c, d);
        graph.addEdge(c, e);
        graph.addEdge(g, i);
        return graph;
    }

    private class ViewHolder {
        TextView textView;
        LinearLayout bgChanged;
        CardView cardView;

        ViewHolder(View view) {
            textView = view.findViewById(R.id.textView);
            bgChanged = view.findViewById(R.id.backgroud);
            cardView = view.findViewById(R.id.card_view);
        }
    }

    protected String getNodeText() {
        return "Node " + nodeCount++;
    }
}
