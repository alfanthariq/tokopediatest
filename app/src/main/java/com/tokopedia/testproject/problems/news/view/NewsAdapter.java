package com.tokopedia.testproject.problems.news.view;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.tokopedia.testproject.R;
import com.tokopedia.testproject.problems.news.model.Article;
import com.tokopedia.testproject.problems.news.model.News;
import com.tokopedia.testproject.problems.news.model.NewsList;
import com.tokopedia.testproject.problems.news.utils.DateOperationUtil;

import java.util.ArrayList;
import java.util.List;

public class NewsAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<NewsList> articleList;
    private final int VIEW_TYPE_ITEM = 0;
    private final int VIEW_TYPE_LOADING = 1;
    private final int VIEW_TYPE_HEADER = 2;

    NewsAdapter(List<NewsList> articleList) {
        setArticleList(articleList);
    }

    void setArticleList(List<NewsList> articleList) {
        if (articleList == null) {
            this.articleList = new ArrayList<>();
        } else {
            this.articleList = articleList;
        }
    }

    void addLoading(){
        articleList.add(null);
        notifyItemInserted(articleList.size() - 1);
    }

    void removeLoading(){
        if (!articleList.isEmpty()) {
            int lastIdx = articleList.size() - 1;
            if (articleList.get(lastIdx) == null) {
                articleList.remove(lastIdx);
                notifyItemRemoved(lastIdx);
            }
        }
    }

    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        if (i == VIEW_TYPE_ITEM) {
            return new NewsViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_news, viewGroup, false));
        } else if (i == VIEW_TYPE_HEADER) {
            return new HeaderViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_header_list, viewGroup, false));
        } else {
            return new LoadingViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.item_loading, viewGroup, false));
        }
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        if (viewHolder instanceof NewsViewHolder) {
            ((NewsViewHolder) viewHolder).bind(articleList.get(position));
        } else if (viewHolder instanceof HeaderViewHolder) {
            ((HeaderViewHolder) viewHolder).bind(articleList.get(position));
        } else if (viewHolder instanceof LoadingViewHolder) {
            showLoadingView((LoadingViewHolder) viewHolder, position);
        }

    }

    private void showLoadingView(LoadingViewHolder viewHolder, int position) {
        //ProgressBar would be displayed
    }

    @Override
    public int getItemCount() {
        return articleList.size();
    }

    @Override
    public int getItemViewType(int position) {
        int viewType = 0;
        if (articleList.get(position) == null) {
            viewType = VIEW_TYPE_LOADING;
        } else {
            if (articleList.get(position).getDataType() == 0)
                viewType = VIEW_TYPE_HEADER;
            else
                viewType = VIEW_TYPE_ITEM;
        }
        return viewType;
    }

    private class LoadingViewHolder extends RecyclerView.ViewHolder {

        ProgressBar progressBar;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            progressBar = itemView.findViewById(R.id.progressBar);
        }
    }

    private class HeaderViewHolder extends RecyclerView.ViewHolder {

        TextView txt_title;

        public HeaderViewHolder(@NonNull View itemView) {
            super(itemView);
            txt_title = itemView.findViewById(R.id.txt_header_title);
        }

        void bind(NewsList article) {
            txt_title.setText(article.getBulanTahun());
        }
    }

    private class NewsViewHolder extends RecyclerView.ViewHolder {

        ImageView imageView;
        TextView tvTitle;
        TextView tvDescription;
        TextView tvDate;

        NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            imageView = itemView.findViewById(R.id.iv);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvDescription = itemView.findViewById(R.id.tvDescription);
            tvDate = itemView.findViewById(R.id.tvDate);
        }

        void bind(NewsList article) {
            Glide.with(itemView).load(article.getImgUrl()).into(imageView);
            tvTitle.setText(article.getTitle());
            tvDescription.setText(article.getDescription());
            tvDate.setText(article.getPublishedAt());
        }
    }
}
