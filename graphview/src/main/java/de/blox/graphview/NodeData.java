package de.blox.graphview;

public class NodeData {
    String nama;
    int level;

    public NodeData(String nama){
        this.nama = nama;
    }

    public NodeData(String nama, int level) {
        this.nama = nama;
        this.level = level;
    }

    public String getNama() {
        return nama;
    }

    public int getLevel() {
        return level;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public void setLevel(int level) {
        this.level = level;
    }
}
